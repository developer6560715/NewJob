# Payment Method Test

![alt Console APP](https://gitlab.com/developer6560715/NewJob/-/raw/main/images/UI.jpg)

This code demonstrates implementation cases for educational purposes.
 
```mermaid
graph LR
  A[User Validate] --> B[Pay Method] --> C[Update User]
  C --> D[Trigger observers]
  C --> E[Persist User]
```

#### Implementation User Validation

This project utilizes [FluentValidation](https://docs.fluentvalidation.net/), and the tests are conducted [here](https://gitlab.com/developer6560715/NewJob/-/blob/main/tests/Payment.Domain.Test/UserRegisterTests.cs).

#### Create Dependency Injection

The [ServiceConfigurator](https://gitlab.com/developer6560715/NewJob/-/blob/7ceb4756af3baccf27e35055dcda469dc7aef06e/src/Services/Payment.DependencyServices/ServiceConfigurator.cs) class is a pivotal component that defines the dependency injection for the solution. In the future, such as when adding a new API project, this class can be utilized as a configuration builder

#### Develope Factory Method

The [PaymentService](https://gitlab.com/developer6560715/NewJob/-/blob/main/src/Services/Payment.Method/PaymentService.cs) class employs the use of a factory method to select and create the concrete implementation for the payment types.

#### Establish Observation Mechanism

The [PaymentService](https://gitlab.com/developer6560715/NewJob/-/blob/main/src/Services/Payment.Method/PaymentService.cs) class also defines the observers that contain the events triggered when the User property of PaymentCheck changes.

#### Implement Singleton Manager

The [Logger](https://gitlab.com/developer6560715/NewJob/-/blob/main/src/Services/Payment.Log/Logger.cs) class implements a singleton, ensuring a unique instance is utilized.

## Installation

You can either use the dotnet CLI to build and run the project or download all the code and open Payment.sln in Visual Studio

The project Payment.CLI must be the start up project wich implements [Cocona](https://github.com/mayuki/Cocona). 

## Usage

### CLI

```terminal
dotnet run paymentwith --name Jose --cpf 422.652.200-19 --email email@hotmail.com --type pix
```

Usage: Payment.CLI paymentwith [--name <String>] [--cpf <String>] [--email <String>] [--type <String>] [--help]
  
Options:

| Parameter            | Description                                    |
| ---------------------| ---------------------------------------------- |
| -n, --name <String>  | User name (Required)                           | 
| -d, --cpf <String>   | User CPF (Required)                            |                
| -e, --email <String> | User e-mail (Required)                         |
| -t, --type <String>  | Payment types: visa, pix or paypal (Required)  | 
| -h, --help           | Show help message                              |

### Web API

Run Web APi direct in Visual Studio or run docker-compose (inside /src/):

```terminal
docker-compose up -d
```

Open in the local browser: http://localhost:8080/swagger/index.html

