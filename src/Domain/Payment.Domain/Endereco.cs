﻿namespace Payment.Domain
{
    public class Address
    {
        public string Street { get; private set; }
        public string Number { get; private set; }
        public string ZipCode { get; private set; }
        public string City { get; private set; }
        public string State { get; private set; }
        public Guid ClienteId { get; private set; }

        // EF Relation
        public Address User { get; protected set; }

        public Address(string logradouro, string numero, string complemento, string bairro, string cep, string cidade, string estado)
        {
            Street = logradouro;
            Number = numero;
            ZipCode = cep;
            City = cidade;
            State = estado;
        }
    }
}