﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Domain.Utils
{

    public class ChangedEventArgs<T> : EventArgs
    {
        public T Entity { get; }

        public ChangedEventArgs(T _entity)
        {
            Entity = _entity;
        }
    }

}
