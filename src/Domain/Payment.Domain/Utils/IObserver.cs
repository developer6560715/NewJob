﻿namespace Payment.Domain.Utils
{
    public interface IObserver<T>
    {
        void Changed(object? sender, ChangedEventArgs<T> e);
    }

}
