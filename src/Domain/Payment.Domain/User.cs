﻿
using FluentValidation;
using Payment.Domain.DomainObjects;
using Payment.Domain.Utils;
using System;

namespace Payment.Domain
{
    public class User: Entity 
    {
        private bool paymentCheck;

        public string Name { get; set; }
        public string Email { get; private set; }

        public string BirthDate { get; set; }

        public string Gender { get; set; }

        public Address Address { get; set; }

        public Cpf Cpf { get; set; }
          
        public bool PaymentCheck
        {
            get { return paymentCheck; }
            set
            {
                if (paymentCheck != value)
                {
                    paymentCheck = value;
                    NotifyObservers();
                }
            }
        }


        public User(Guid id, string nome, string email, string cpf)
        {
            Id = id;
            Name = nome;
            Email = email;
            Cpf = new Cpf(cpf);
            PaymentCheck = false;
        }
 
        public void AtribuirEndereco(Address address)
        {
            Address = address;
        }

        public override bool IsValid()
        {
            ValidationResult = new RegistrarClienteValidation().Validate(this);
            return ValidationResult.IsValid;
        }
         
        public event EventHandler<ChangedEventArgs<User>> ClientChanged;
         
        protected virtual void OnUserChanged(ChangedEventArgs<User> e)
        {
            ClientChanged?.Invoke(this, e);
        }

        private void NotifyObservers()
        {
            OnUserChanged(new ChangedEventArgs<User>(this));
        }

        public class RegistrarClienteValidation : AbstractValidator<User>
        {
            public RegistrarClienteValidation()
            {
                RuleFor(c => c.Id)
                    .NotEqual(Guid.Empty)
                    .WithMessage("Id do cliente inválido");

                RuleFor(c => c.Name)
                    .NotEmpty()
                    .WithMessage("The customer's name was not provided");

                RuleFor(c => c.Cpf.Numero)
                    .Must(TerCpfValido)
                    .WithMessage("CPF is invalid.");

                RuleFor(c => c.Email)
                    .EmailAddress()
                    .Must(TerEmailValido)
                    .WithMessage("E-mail is invalid.");
            }

            protected static bool TerCpfValido(string cpf)
            {
                return Payment.Domain.DomainObjects.Cpf.Valid(cpf);
            }

            protected static bool TerEmailValido(string email)
            {
                return Payment.Domain.DomainObjects.Email.Valid(email);
            }
        }
    }
}
