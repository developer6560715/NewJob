﻿using FluentValidation.Results; 

namespace Payment.Domain.DomainObjects
{
    public abstract class Entity
    {
        public Guid Id { get; set; }

        protected Entity()
        {
            Id = Guid.NewGuid();
        }
        public ValidationResult ValidationResult { get; set; }

        public virtual bool IsValid()
        {
            return false;
        }
    }
}