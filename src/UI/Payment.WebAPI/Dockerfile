
FROM mcr.microsoft.com/dotnet/aspnet:8.0 AS base
USER app
WORKDIR /app 
EXPOSE 5000
EXPOSE 5001

FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
ARG BUILD_CONFIGURATION=Release
WORKDIR /src
COPY ["/UI/Payment.WebAPI/Payment.WebAPI.csproj", "UI/Payment.WebAPI/"]
COPY ["/Services/Payment.DependencyServices/Payment.DependencyServices.csproj", "Services/Payment.DependencyServices/"] 
COPY ["/Services/Payment.Register/*.csproj", "Services/Payment.Register/"]
COPY ["/Services/Payment.Method/*.csproj", "Services/Payment.Method/"]
COPY ["/Services/Payment.Log/*.csproj", "Services/Payment.Log/"]
COPY ["/Services/Payment.AntiCorruption/*.csproj", "Services/Payment.AntiCorruption/"]
COPY ["/Domain/Payment.Domain/*.csproj", "Domain/Payment.Domain/"]

RUN dotnet restore "./UI/Payment.WebAPI/Payment.WebAPI.csproj"
COPY . .
WORKDIR "/src/UI/Payment.WebAPI"
RUN dotnet build "./Payment.WebAPI.csproj" -c $BUILD_CONFIGURATION -o /app/build

FROM build AS publish
ARG BUILD_CONFIGURATION=Release
RUN dotnet publish "./Payment.WebAPI.csproj" -c $BUILD_CONFIGURATION -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Payment.WebAPI.dll"]