using Microsoft.Extensions.DependencyInjection;
using Payment.DependencyServices;
using Payment.Domain;
using Payment.Domain.DomainObjects;
using Payment.Method;
using Payment.Method.Interface;
using System.Xml.Linq;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.Configure();

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();




app.MapPost("/user", async (IPaymentService paymentService, string type, string nome, string email, string cpf) =>
{
   paymentService.User = new Payment.Domain.User(Guid.NewGuid(), nome, email, cpf);
   paymentService.Type = type;

   var result = await paymentService.Send();
   return result.Message;
})
.WithName("user")
.WithOpenApi();
app.Run();


