﻿ 
using Cocona;  
using Payment.DependencyServices;


var builder = CoconaApp.CreateBuilder(); 
ServiceConfigurator.Configure(builder.Services);
 
var app = builder.Build();
 
app.AddCommands<PaymentCommands>(); 

app.Run();

 
 