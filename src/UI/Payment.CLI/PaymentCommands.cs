﻿using Cocona;
using Payment.Domain;
using Payment.Log;
using Payment.Method;
using Payment.Method.Interface;
using Payment.Register.Interface;
using Spectre.Console;

public class PaymentCommands
{
    private readonly IPaymentService _paymentService;

    public PaymentCommands(IPaymentService paymentService)
    {
        _paymentService = paymentService;
    }

    [Command("paymentwith")]
    public async Task With([Option('n', Description = "User name")] string name, [Option('d', Description = "User CPF")]  string cpf, [Option('e', Description = "User e-mail")] string email, [Option('t', Description = "Payment types: visa, pix or paypal")]  string type)
    {
        try
        {
            Console.Clear();
            AnsiConsole.Write(
                       new FigletText("New Job - Payment Exercices")
                           .LeftJustified()
                           .Color(Color.White));


             
            _paymentService.User = new Payment.Domain.User(Guid.NewGuid(), name, email, cpf);
            _paymentService.Type = type;
            var result = await _paymentService.Send(); 
            AnsiConsole.MarkupLine($"[blue]{Emoji.Known.CheckMark}  -  {result.Message}[/]");

        }
        catch (Exception ex)
        {
            AnsiConsole.MarkupLine($"[red]{Emoji.Known.SadButRelievedFace}[/]");
            Logger.GetInstance().Error("Error occurs", ex);
            AnsiConsole.WriteLine();  
            AnsiConsole.WriteException(ex, ExceptionFormats.ShortenPaths);
        }

    }


}