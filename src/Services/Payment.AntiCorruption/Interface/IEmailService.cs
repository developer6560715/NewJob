﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.AntiCorruption.Interface
{
    public interface IEmailService
    {
        Task Send(string email);
    }
}
