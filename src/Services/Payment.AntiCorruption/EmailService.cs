﻿using Payment.AntiCorruption.Interface;

namespace Payment.AntiCorruption
{
    public class EmailService : IEmailService
    {
        public Task Send(string email)
        {
            Console.WriteLine($" -> send to {email}");
            return Task.CompletedTask;
        }
    }
}
