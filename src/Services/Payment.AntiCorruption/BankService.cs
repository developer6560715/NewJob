﻿using Payment.AntiCorruption.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.AntiCorruption
{
    public class BankService : IBankService
    {
        public Task Send(string bankName)
        {
            Console.WriteLine($" -> send to bank {bankName}");
            return Task.CompletedTask;
        }
    }
}
