﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Log
{
    public class Logger
    {
        private static Logger instance;
        private Logger() { }
 
        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger();
            }
            return instance;
        }

        public void Log(string message)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine($"[INFO] {DateTime.Now}: {message}");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void Error(string message, Exception ex)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"[ERR] {DateTime.Now}: {message} - {ex.Message}");
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
