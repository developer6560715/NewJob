﻿using Microsoft.Extensions.DependencyInjection;
using Payment.AntiCorruption;
using Payment.AntiCorruption.Interface;
using Payment.Method;
using Payment.Method.Interface;
using Payment.Register;
using Payment.Register.Interface;


namespace Payment.DependencyServices
{

    public static class ServiceConfigurator
    {
        public static void Configure(this IServiceCollection services)
        {
            services.AddTransient<IUserRegisterService, UserRegisterService>();
            services.AddTransient<IPaymentService, PaymentService>();
            services.AddTransient<IBankService, BankService>();
            services.AddTransient<IEmailService, EmailService>();

        }
    }
}
