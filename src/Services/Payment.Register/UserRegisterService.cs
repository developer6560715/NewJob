﻿using Payment.Domain;
using Payment.Domain.DomainObjects;
using Payment.Register.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Register
{
    public sealed class UserRegisterService : IUserRegisterService
    {
        public ResultOperation Save(User user)
        {
            return new ResultOperation() { DateOperation = DateTime.Now , Message = $"User : {user.Name} with {user.Cpf.Numero} saved"};
        }
    }
}
