﻿using Payment.Domain;
using Payment.Domain.DomainObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Register.Interface
{
    public interface IUserRegisterService
    {
        public ResultOperation Save(User user); 
    }
}
