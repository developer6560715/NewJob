﻿using Payment.AntiCorruption.Interface;
using Payment.Domain;
using Payment.Domain.Utils;
using Payment.Method.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Method.Observer
{

    public class UserBankObserver : Domain.Utils.IObserver<User>
    {
        readonly IBankService _bankService;
        public UserBankObserver(IBankService bankService)
        {
            _bankService = bankService;
        }

        public async void Changed(object? sender, ChangedEventArgs<User> e)
        {
            await _bankService.Send("Itau");
            Console.WriteLine($"Send bank: {e.Entity.Name}");
        }
         
    }
}
