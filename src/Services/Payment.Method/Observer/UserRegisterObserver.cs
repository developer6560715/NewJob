﻿using Payment.AntiCorruption.Interface;
using Payment.Domain;
using Payment.Domain.Utils;
using Payment.Method.Interface;
using Payment.Register.Interface;

namespace Payment.Method.Observer
{
    public class UserRegisterObserver : Domain.Utils.IObserver<User>
    {

        readonly IUserRegisterService _userRegisterService;
        public UserRegisterObserver(IUserRegisterService userRegisterService)
        {
            _userRegisterService = userRegisterService;
        }

        public void Changed(object? sender, ChangedEventArgs<User> e)
        {
            var result = _userRegisterService.Save(e.Entity);
            Console.WriteLine(result.Message);
        }



    }
}
