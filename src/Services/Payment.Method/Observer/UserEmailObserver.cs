﻿using Payment.AntiCorruption.Interface;
using Payment.Domain;
using Payment.Domain.Utils;
using Payment.Method.Interface;

namespace Payment.Method.Observer
{
    public class UserEmailObserver : Domain.Utils.IObserver<User>
    {

        readonly IEmailService _emailService;
        public UserEmailObserver(IEmailService emailService)
        {
            _emailService = emailService;
        }

        public async void Changed(object? sender, ChangedEventArgs<User> e)
        {
            await _emailService.Send(e.Entity.Email);
            Console.WriteLine($"Send email : {e.Entity.Name}");
        }
         
    }
}
