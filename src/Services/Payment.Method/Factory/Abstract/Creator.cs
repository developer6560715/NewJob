﻿using Payment.AntiCorruption.Interface;
using Payment.Domain;
using Payment.Domain.DomainObjects; 
using Payment.Log;
using Payment.Method.Factory.Interface; 
using Payment.Method.Observer;

namespace Payment.Method.Factory.Abstract
{
    public abstract class Creator
    {
 
        public abstract IPayment FactoryMethod();

        public async Task<ResultOperation> Send(User user)
        { 

            var payMethod = FactoryMethod();
            payMethod.User = user;
            payMethod.Send(); 

            Logger logger = Logger.GetInstance();

            payMethod.User.PaymentCheck = true; 

            var result = new ResultOperation() { DateOperation = DateTime.Now, Message = $"Payment {payMethod.User.Name} with {payMethod.Type}", Sucess = true };
            
             
            logger.Log($"Logger: {result.Message}");

            return result;
        }
 
    }
}
