﻿using Payment.Domain;
using Payment.Method.Factory.Interface;
using Payment.Method.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Method.Factory.Type
{
    class PayPal : IPayment
    {
        public User User { get; set; }
        public string Type { get; set; }

        void IPayment.Send()
        {
            Type = "PayPal";
            Console.WriteLine("Payment with PayPal");
        }
    }
}
