﻿using Payment.Domain;
using Payment.Method.Factory.Interface;
using Payment.Method.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Method.Factory.Type
{
    class Pix : IPayment
    {
        public User User { get; set; }
        public string Type { get; set; }

        void IPayment.Send()
        {
            Type = "PIX";
            Console.WriteLine("Payment with PIX");
        }
    }
}
