﻿using Payment.Domain;
using Payment.Method.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Method.Factory.Interface
{
    public interface IPayment
    {
        public string Type { get; set; }
        public User User { get; set; }
        void Send();
    }
}
