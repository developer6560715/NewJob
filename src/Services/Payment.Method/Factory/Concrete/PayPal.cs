﻿using Payment.Domain;
using Payment.Method.Factory.Abstract;
using Payment.Method.Factory.Interface;
using Payment.Method.Interface;


namespace Payment.Method.Factory.Concrete
{
    public class PayPal : Creator
    {
        public override IPayment FactoryMethod()
        {
            return new Factory.Type.PayPal();
        }
    }
}
