﻿using Payment.Domain;
using Payment.Method.Factory.Abstract;
using Payment.Method.Factory.Interface;
using Payment.Method.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Method.Factory.Payment
{
    public class Visa : Creator
    {
        public override IPayment FactoryMethod()
        {
            return new Factory.Type.Visa();
        }
    }
}
