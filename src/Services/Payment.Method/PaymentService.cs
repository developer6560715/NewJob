﻿using Payment.AntiCorruption.Interface;
using Payment.Domain;
using Payment.Domain.DomainObjects;
using Payment.Domain.Utils;
using Payment.Method.Factory.Abstract;
using Payment.Method.Factory.Concrete;
using Payment.Method.Factory.Payment;
using Payment.Method.Interface;
using Payment.Method.Observer;
using Payment.Register.Interface;

namespace Payment.Method
{
    public class PaymentService : IPaymentService
    { 

        private readonly IBankService _bankService;
        private readonly IEmailService _emailService;
        private readonly IUserRegisterService _userRegisterService;
        public PaymentService(IBankService bankService, IEmailService emailService, IUserRegisterService userRegisterService)
        {
            _bankService = bankService;
            _emailService = emailService;
            _userRegisterService = userRegisterService;

        }

        private Creator? creator; 
 
        public User User { get; set; }

        public string Type { get; set; }
        public async Task<ResultOperation> Send()
        {

            if (! User.IsValid() ) { throw new ArgumentException(User.ValidationResult.Errors.FirstOrDefault().ErrorMessage); }

            switch (Type)
            {
                case "visa":
                    creator = new Visa();
                    break;
                case "pix":
                    creator = new Pix();
                    break;
                case "paypal":
                    creator = new PayPal();
                    break; 
            }

            if (creator == null) { throw new ArgumentException("Payment type not implemented"); }

            Domain.Utils.IObserver<User> bank = new UserBankObserver(_bankService);
            Domain.Utils.IObserver<User> email = new UserEmailObserver(_emailService);
            Domain.Utils.IObserver<User> register = new UserRegisterObserver(_userRegisterService);

            User.ClientChanged += bank.Changed;
            User.ClientChanged += email.Changed;
            User.ClientChanged += register.Changed;

             

            return await creator?.Send(User);
        }
         
    }
}
