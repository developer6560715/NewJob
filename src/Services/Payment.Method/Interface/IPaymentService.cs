﻿using Payment.Domain;
using Payment.Domain.DomainObjects;
using System.Reflection.Metadata;
using System.Threading.Tasks;

namespace Payment.Method.Interface
{
    public interface IPaymentService
    {
        public string Type { get; set; }
        public User User { get; set; }
        Task<ResultOperation> Send();
    }
}
