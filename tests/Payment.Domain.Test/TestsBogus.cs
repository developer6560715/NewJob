﻿using Bogus;
using Bogus.DataSets;
using Bogus.Extensions.Brazil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Payment.Domain.Test
{
    [CollectionDefinition(nameof(UserBogusCollection))]
    public class UserBogusCollection : ICollectionFixture<UserTestsBogusFixture>
    { }

    public class UserTestsBogusFixture : IDisposable
    {
        public User CreateValidUsers()
        {
            return CreateUsers(1, true).FirstOrDefault();
        }

        public IEnumerable<User> ObterUsersVariados()
        {
            var users = new List<User>();

            users.AddRange(CreateUsers(50, true).ToList());
            users.AddRange(CreateUsers(50, false).ToList());

            return users;
        }

        public IEnumerable<User> CreateUsers(int quantidade, bool ativo)
        {
            var genero = new Faker().PickRandom<Name.Gender>();
             

            var Users = new Faker<User>("pt_BR")
                .CustomInstantiator(f => new User(
                    Guid.NewGuid(),
                    $"{f.Name.FirstName(genero)} {f.Name.LastName(genero)}",
                    "",
                    f.Person.Cpf(true))).RuleFor(c => c.Email, (f,c) =>
                    f.Internet.Email(c.Name, c.Name)); 

            return Users.Generate(quantidade);
        }

        public User CreateUserInvalido()
        {
            var genero = new Faker().PickRandom<Name.Gender>();

            var user = new Faker<User>("pt_BR")
                .CustomInstantiator(f => new User(
                    Guid.NewGuid(),
                    $"{f.Name.FirstName(genero)} {f.Name.LastName(genero)}",
                    f.Person.Cpf(true),
                    "")).RuleFor(c => c.Email, (f, c) =>
                    f.Internet.Email(c.Name, c.Name));

            return user;
        }

        public void Dispose()
        {
        }
    }
}
