using Bogus;
using Bogus.Extensions.Brazil; 

namespace Payment.Domain.Test
{
    [Collection(nameof(UserBogusCollection))]
    public class UserRegisterTests
    {

        private readonly UserTestsBogusFixture _userTestsFixture;

        public UserRegisterTests(UserTestsBogusFixture userTestsFixture)
        {
            _userTestsFixture = userTestsFixture;
        }
         
        [Trait("User", "Valid Users")]
        [Fact]
        public void User_All_IsValid()
        {

            // Arrange
            var cliente = _userTestsFixture.CreateValidUsers();

            // Act
            var result = cliente.IsValid();

            // Assert 
            Assert.True(result);
            Assert.Empty(cliente.ValidationResult.Errors);
        }

        [Trait("User", "Valid e-mails")] 
        [InlineData("email1@domain.com")]
        [InlineData("email1@domain.com.us")]
        [InlineData("user@domain.com.br")] 
        public void User_Email_IsValid(string email)
        {

            // Arrange
            var faker = new Faker("pt_BR");  
            var user = new User(Guid.NewGuid(), faker.Name.FullName(Bogus.DataSets.Name.Gender.Male), email, faker.Person.Cpf(true));

            // Act
            var result = user.IsValid();

            // Assert
            Assert.True(result);
        }

        [Trait("User", "Invalid e-mails")]
        [Theory]
        [InlineData("email1_domain.com")]
        [InlineData("email1@domain")]
        [InlineData("user@domain@com.br")]
        public void User_Email_IsInValid(string email)
        {
            // Arrange
            var faker = new Faker("pt_BR");
            var user = new User(Guid.NewGuid(), faker.Name.FullName(Bogus.DataSets.Name.Gender.Male), email, faker.Person.Cpf(true));

            // Act
            var result = user.IsValid();

            // Assert
            Assert.False(result);
        }

        [Trait("User", "Invalid users")]
        [Fact]
        public void User_FieldsRequired_IsValid()
        {

            // Arrange
            var faker = new Faker("pt_BR");
            var user = new User(Guid.NewGuid(), string.Empty, string.Empty, faker.Person.Cpf(true));

            // Act
            var result = user.IsValid();

            // Assert
            Assert.False(result);
        }
    }
}