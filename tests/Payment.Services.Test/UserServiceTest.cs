using Bogus;
using Payment.AntiCorruption.Interface;
using Payment.AntiCorruption;
using Payment.Method;
using Payment.Method.Factory.Abstract;
using Payment.Method.Factory.Payment;
using Payment.Domain;
using Bogus.Extensions.Brazil;
using Moq;
using Moq.AutoMock;
using Payment.Method.Interface;
using Payment.Register.Interface;
using Payment.Register;

namespace Payment.Services.Test
{
    public class UserServiceTest
    {
 

        [Fact]
        public async Task User_Register_IfIsValidAsync()
        {
            // Arrange
            var Mocker = new AutoMocker();
            var bankService =  Mocker.CreateInstance<BankService>();
            var emailService = Mocker.CreateInstance<EmailService>();
            var userRegisterService = Mocker.CreateInstance<UserRegisterService>(); 

            PaymentService factory = new PaymentService(bankService, emailService, userRegisterService);
            var faker = new Faker("pt_BR");
            var name = faker.Name.FullName(Bogus.DataSets.Name.Gender.Male);
            factory.Type = "visa";
            factory.User = new User(Guid.NewGuid(), name, faker.Person.Email, faker.Person.Cpf(true));
            // Act
            var result = await factory.Send();

            // Assert 
            Assert.Equal(result.Message, $"Payment {name}");
        }

         

    }
}