﻿ 
using Payment.Method.Factory.Abstract;
using Payment.Method.Factory.Payment; 
using Bogus;
using Bogus.Extensions.Brazil;

namespace Payment.Services.Test
{
    public class PaymentServiceTest
    {
        [Fact]
        public async Task Payment_SelectVisaAsync()
        {
            // Arrange
            Creator factory = new Visa();
            var faker = new Faker("pt_BR");
            var name = faker.Name.FullName(Bogus.DataSets.Name.Gender.Male);

            // Act
            var result = await factory.Send(new Domain.User(Guid.NewGuid(), name, faker.Person.Email, faker.Person.Cpf(true)));

            // Assert 
            Assert.Equal(result.Message, $"Payment {name}"); 


        }

    }
}
